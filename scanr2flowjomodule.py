'''
# File Name: scanr2flowjomodule.py
# 
# Date: Summer 2014
#
# Module for converting ScanR .csv files to smaller chuncks of data for Flowjo use.
# Additionally contains functions to setup a predefined file structure to improve 
# organization of data in Flowjo as well as Keyword metadata out of the plate layout file 
#
'''

# import 
import csv
import glob  
import os
import sys
import linecache

# modules 
def GetScanrMainFileNames(s_scanr_file_path): 
    '''
    Fetch a list of input files name saccoring to s_scanr_file_path pattern 
    '''
    print('i GetScanrMainFileNames s_scanr_file_path :', s_scanr_file_path)
    l_scanr_main_file_name=glob.glob(s_scanr_file_path)
    print('o GetScanrMainFileNames l_scanr_main_file_name :', l_scanr_main_file_name)
    return(l_scanr_main_file_name)


def PlateLayoutFile2PlateLayoutDictionary(s_plate_layout, ls_keyword_label, s_wellindex_label='WellIndex'):
    '''
     Takes platelayout file, and creates a dictionary out of them for future use
     Returns dictionary. Key is well. 
    '''
    print('i PlateLayoutFile2PlateLayoutDictionary s_plate_layout :', s_plate_layout)
    print('i PlateLayoutFile2PlateLayoutDictionary ls_keyword_label :', ls_keyword_label)
    print('i PlateLayoutFile2PlateLayoutDictionary s_wellindex_label :', s_wellindex_label)
    # make empty plate layout dictionary 
    d_layout={}
    i_key = None
    # open a platelayout file 
    with open(s_plate_layout,'Ur') as f:
        reader=csv.reader(f, delimiter='\t')
        # for each row of plate layout file 
        for ls_row in reader: 
            # if first row 
            if reader.line_num == 1: 
                # get key
                i_key = ls_row.index(s_wellindex_label)
                # get value
                li_value = []
                for s_keyword_label in ls_keyword_label: 
                    i_value = ls_row.index(s_keyword_label)
                    li_value.append(i_value)
                ti_value = tuple(li_value)
            #if other row then first row
            else: 
                s_key = ls_row[i_key]
                ls_value = [ls_row[i_value] for i_value in li_value]
                ts_value = tuple(ls_value) 
                d_layout.update({s_key : ts_value})
    # return dictionary 
    print('o PlateLayoutFile2PlateLayoutDictionary d_layout :', d_layout)
    return (d_layout)


def Scanr2FlowjoKeywordEntry(s_well, s_flowjo_file_name, s_plate_layout, dd_metalayout):
    print('i Scanr2FlowjoKeywordEntry s_well :', s_well)
    print('i Scanr2FlowjoKeywordEntry s_flowjo_file_name :', s_flowjo_file_name)
    print('i Scanr2FlowjoKeywordEntry s_plate_layout :', s_plate_layout)
    print('i Scanr2FlowjoKeywordEntry dd_metalayout : print suppresed ')
    #print('i Scanr2FlowjoKeywordEntry dd_metalayout :', dd_metalayout)
    # processing
    ls_keyword=[]
    ls_keyword = list(dd_metalayout[s_plate_layout][s_well])
    ls_keyword.insert(0,s_flowjo_file_name) 
    # output 
    print('o Scanr2FlowjoKeywordEntry ls_keyword :', ls_keyword)
    return (ls_keyword) 


def FlowjoFileWriter(s_well, s_cellline_stainingset_platelayout, s_flowjo_file_name, s_flowjo_file_path, s_plate_layout, t_label_flowjo, l_baseflowjo_data, dd_metalayout): 
    print ('i FlowjoFileWriter s_well :', s_well)
    print ('i FlowjoFileWriter s_flowjo_file_name :', s_cellline_stainingset_platelayout)
    print ('i FlowjoFileWriter s_flowjo_file_name :', s_flowjo_file_name)
    print ('i FlowjoFileWriter s_flowjo_file_path :', s_flowjo_file_path)
    print ('i FlowjoFileWriter s_plate_layout :', s_plate_layout)
    print ('i FlowjoFileWriter t_label_flowjo :', t_label_flowjo)
    print ('i FlowjoFileWriter l_baseflowjo_data :', l_baseflowjo_data)
    print ('i FlowjoFileWriter dd_metalayout : print suppresed')
    #print ('i FlowjoFileWriter dd_metalayout :', dd_metalayout)
    # handle input 
    s_flowjo_full_path_file = s_flowjo_file_path+'/'+s_flowjo_file_name
    s_keyword_full_path_file = s_flowjo_file_path+'/Keyword_'+s_cellline_stainingset_platelayout+'.csv'
    # check if output path already exist, make if not 
    if not os.path.isdir(s_flowjo_file_path):
        os.makedirs(s_flowjo_file_path)
    # check if file alredy exist 
    if not os.path.isfile(s_flowjo_full_path_file):
        # make flowjo file and write labels 
        print(s_flowjo_full_path_file)
        with open(s_flowjo_full_path_file,'w') as f:
            label_writer=csv.writer(f, delimiter=',')
            label_writer.writerow(t_label_flowjo)
        # writre keyword file entry, make file if it is not existng  
        ls_keywords=Scanr2FlowjoKeywordEntry(s_well, s_flowjo_file_name, s_plate_layout, dd_metalayout)
        with open(s_keyword_full_path_file,'a+') as k:
            k_writer=csv.writer(k, delimiter = ',')
            k_writer.writerow(ls_keywords) 
    # wrire flowjo data file entry  
    with open(s_flowjo_full_path_file,'a') as g:   
        data_writer=csv.writer(g, delimiter=',')
        data_writer.writerow(l_baseflowjo_data)


def Scanr2FlowjoMainEntry(s_scanr_file_path, t_cell_line, t_stain_set, dd_staining, d_plate_layout, dd_metalayout):
    print('i Scanr2FlowjoMainEntry s_scanr_file_path :', s_scanr_file_path)
    print('i Scanr2FlowjoMainEntry s_cell_line :', t_cell_line)
    print('i Scanr2FlowjoMainEntry t_stain_set :', t_stain_set)
    print('i Scanr2FlowjoMainEntry dd_staining :', dd_staining)
    print('i Scanr2FlowjoMainEntry s_plate_layout  :', d_plate_layout)
    print('i Scanr2FlowjoMainEntry dd_metalayout  :', dd_metalayout)

    # set variables 
    i_uberfile_index = 1  # set read file line number index to 1

    # for each celline
    for cell_line in t_cell_line: 
        if cell_line in s_scanr_file_path:
            s_cell_line=cell_line
 
    # fore each plate layout     
    for plate_layout in d_plate_layout.keys(): 
        if plate_layout in s_scanr_file_path:
            s_plate_layout=plate_layout
    
    # get uberfile length
    i_uberfile_length = 1
    for line in open(s_scanr_file_path):
        i_uberfile_length +=1

    # get scanr main coordiantes 
    # note : s_scanr_file_path is always the scanr filen name for Main
    # scanar main file name 
    s_scanr_file_path_main = s_scanr_file_path 
    # scanr main label 
    main = linecache.getline(s_scanr_file_path_main, 1)
    l_label_scanr_main =[x.strip(' \n') for x in main.split('\t')]
    i_well_position_main = l_label_scanr_main.index('Well')
    i_objectid_position_main = l_label_scanr_main.index('Object ID') 
    
    # scanr cyto coordimates
    # scanr cyto file name 
    s_scanr_file_path_cyto=s_scanr_file_path.replace('Main','Cyto10')
    # scanr cyto label 
    try:
        r_cyto=open(s_scanr_file_path_cyto, 'Ur')
        cyto = linecache.getline(s_scanr_file_path_cyto, 1)
        l_label_scanr_cyto = [x.strip(' \n') for x in cyto.split('\t')]
        i_well_position_cyto = l_label_scanr_cyto.index('Well') 
        i_objectid_position_cyto = l_label_scanr_cyto.index('Object ID') 
    except FileNotFoundError:
        l_label_scanr_cyto = None
        i_well_position_cyto = None
        i_objectid_position_cyto = None
    
    # set uberfile index to 2 the first data line,
    i_uberfile_index +=1
    

    # for each uber scanr file line
    while i_uberfile_index < i_uberfile_length:

        # set get base flowjo data flag
        b_get_baseflowjo_data = True

        # get the uber files staining set 
        for s_stain_set in t_stain_set:
            if s_stain_set in s_scanr_file_path:            
               s_the_stain_set = s_stain_set

        # by first scanar data line convert wavelength to endpoint 
        if i_uberfile_index == 2:
            l_label_flowjo = l_label_scanr_main
            for s_endpoint in dd_staining[s_the_stain_set].keys():
                s_wavelength=dd_staining[s_the_stain_set][s_endpoint][1]
                l_label_flowjo=[w.replace(s_wavelength,s_endpoint) for w in l_label_flowjo]
            t_label_flowjo = tuple(l_label_flowjo)
                              
        # for each endpoint in the staining set 
        for s_endpoint in dd_staining[s_the_stain_set].keys():
                                    
            # get staining location and staining wavelength
            s_staining_location = dd_staining[s_the_stain_set][s_endpoint][0]
            s_staining_wavelength = dd_staining[s_the_stain_set][s_endpoint][1]

            # get main line
            main = linecache.getline(s_scanr_file_path_main, i_uberfile_index)
            l_mainline=[x.strip(' \n') for x in main.split('\t')]

            # if main is staining location
            if s_staining_location == 'Main':
                # get scanr and flowjo endpoint positions 
                i_total_endpoint_position_scanr = l_label_scanr_main.index('Total Intensity '+s_staining_wavelength)   
                i_mean_endpoint_position_scanr = l_label_scanr_main.index('Mean Intensity '+s_staining_wavelength)   
                i_total_endpoint_position_flowjo = i_total_endpoint_position_scanr
                i_mean_endpoint_position_flowjo = i_mean_endpoint_position_scanr

                # get uberline 
                l_uberline = l_mainline

            # if cyto is staining loacation
            else:
                # get scanr and flowjo endpoint positions 
                i_total_endpoint_position_scanr = l_label_scanr_cyto.index('Total Intensity '+s_staining_wavelength)   
                i_mean_endpoint_position_scanr = l_label_scanr_cyto.index('Mean Intensity '+s_staining_wavelength)
                i_total_endpoint_position_flowjo = l_label_scanr_main.index('Total Intensity '+s_staining_wavelength)
                i_mean_endpoint_position_flowjo = l_label_scanr_main.index('Mean Intensity '+s_staining_wavelength)
                
                # get cyto line 
                cyto = linecache.getline(s_scanr_file_path_cyto,i_uberfile_index)
                l_cytoline=[x.strip(' \n') for x in cyto.split('\t')]
                
                # Check if Wells match
                if l_cytoline[i_well_position_cyto] != l_mainline[i_well_position_main]:
                    sys.exit('Error: wells do not match between Cyto file'+l_cytoline[i_well_position_cyto]+' and Main file'+l_mainline[i_well_position_main]+' at Uberfile line '+ str(i_uberfile_index))
                # Check if Object ID matches
                if l_cytoline[i_objectid_position_cyto] != l_mainline[i_objectid_position_main]:
                    sys.exit('Error: object_id  do not match between Cyto file'+l_cytoline[i_objectid_position_cyto]+' and Main file'+l_mainline[i_objectid_position_main]+' at Uberfile line '+ str(i_uberfile_index))

                # get uberline
                l_uberline = l_cytoline 
                
            # check get base flowjo data flag
            if b_get_baseflowjo_data: 
                # get scanar data, set flowjo base data
                l_baseflowjo_data=l_mainline
                b_get_baseflowjo_data = False
            else: 
                # get scanar data, replace flowjo base data parts  
                l_baseflowjo_data[i_total_endpoint_position_flowjo] = l_uberline[i_total_endpoint_position_scanr]
                l_baseflowjo_data[i_mean_endpoint_position_flowjo] = l_uberline[i_mean_endpoint_position_scanr]
                    
        # get well index
        s_well=l_baseflowjo_data[i_well_position_main]
        #  generate s_cellline_stainingset_platelayout string 
        s_cellline_stainingset_platelayout = s_cell_line+'_'+s_the_stain_set+'_'+s_plate_layout
        # generate flowjo file name using cell_line/plate_layout/staining_set/well .csv
        s_flowjo_file_name=s_cell_line+'_'+s_the_stain_set+'_'+s_plate_layout+'_'+'Well'+s_well+'.csv'
        # generate flowjo file path
        s_flowjo_file_path='output/'+s_cell_line+'/'+s_the_stain_set+'/'+s_plate_layout    
        # write one line to one flowjo file 
        FlowjoFileWriter(s_well, s_cellline_stainingset_platelayout, s_flowjo_file_name, s_flowjo_file_path, s_plate_layout, t_label_flowjo, l_baseflowjo_data, dd_metalayout)

        # increment i_uberfile_index
        i_uberfile_index += 1
            
    # return 
    return() 


def Scanr2FlowjoFiles(l_scanr_file_path, t_cell_line, t_stain_set, dd_staining, d_plate_layout, ls_keyword_label):
    print('i Scanr2FlowjoFiles l_scanr_file_path :', l_scanr_file_path)
    print('i Scanr2FlowjoFiles t_cell_line :', t_cell_line)
    print('i Scanr2FlowjoFiles t_stain_set :', t_stain_set)
    print('i Scanr2FlowjoFiles dd_staining :', dd_staining)
    print('i Scanr2FlowjoFiles d_plate_layout :', d_plate_layout)

    #set variables to initial value
    dd_metalayout = {}
    lt_keywords=[]

    # make metalayout  dictionary of dictionary
    print('\n### build metalayout dictionary of dictionary ###')
    for s_plate_layout in d_plate_layout.keys():
        s_plate_layout_file=d_plate_layout[s_plate_layout][0]
        d_layout =  PlateLayoutFile2PlateLayoutDictionary(s_plate_layout_file, ls_keyword_label)
        dd_metalayout.update({s_plate_layout : d_layout})

    # translate scanr files to flowjo files
    for s_scanr_file_path in l_scanr_file_path:
        print('\n### handle scanar file :', s_scanr_file_path, '###')
        for s_cell_line in t_cell_line:  
            if s_cell_line in s_scanr_file_path: 
                Scanr2FlowjoMainEntry(s_scanr_file_path, t_cell_line, t_stain_set, dd_staining, d_plate_layout, dd_metalayout) 
