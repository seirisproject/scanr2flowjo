"""
This main skeleton, which will make use from the scanr2flowjomodule library, have to be addapted to particular settings of each scanar study.  
"""

import scanr2flowjo.scanr2flowjomodule as sf

# set variables
s_scanr_file_path='/My/Path/to/ScanR/Raw/Data/*/*_Main.txt'
t_cell_line=('CELLLINE1','CELLINE2','CELLLINE3',)
t_stain_set=('STAININGSET1','STAININGSET2',)

# s_staining_set : ('s_staining','s_staining_location',s_staining_wavelength) 
dd_staining={
'STAININGSET1' : {'DAPI' : ('Main','DAPI',),
'ENDPOINT1' : ('Main','Alexa 488',),
'ENDPOINT2' : ('Main','Alexa 647',)},

'STAININGSET2' : {'DAPI' : ('Main','DAPI',),
'ENDPOINT3' : ('Main','Alexa 568',),
'ENDPOINT4' : ('Cyto10','Alexa 488',),
'ENDPOINT5' : ('Cyto10','Alexa 647',)},
}
      
# s_plate_layout : s_plate_layout_file : s_plate_layout_tag
d_plate_layout = { 
'drug1' : ('DRUG1_layout_v1.txt','druglayoutplate1'),
#'drug2' : ('DRUG2_layout_v1.txt','druglayoutplate2'),
}

# set columns which will become keyword, WellIndex is always required what so ever
ls_keyword_label = ['WellIndex','Well00','Well','Compound', 'CompoundConcentration_nM', 'CompoundConcentrationRank',]


# processing  
l_scanr_file_path = sf.GetScanrMainFileNames(s_scanr_file_path=s_scanr_file_path)
print('l_scanr_file_path:', l_scanr_file_path)
sf.Scanr2FlowjoFiles(l_scanr_file_path=l_scanr_file_path, t_cell_line=t_cell_line, t_stain_set=t_stain_set,dd_staining=dd_staining, d_plate_layout=d_plate_layout,ls_keyword_label=ls_keyword_label)
    

# self test 
if (__name__=='__main__'):
    print('\n*** selftest s_scanr_file_path ***')
    print('s_scanr_file_path :', s_scanr_file_path)

    print('\n*** selftest t_cell_line ***')
    print('t_cell_line :', t_cell_line)

    print('\n*** selftest t_stain_set ***')
    print('t_stain_set :', t_stain_set)

    print('\n*** selftest dd_staining ***')
    for s_staining_set in dd_staining.keys(): 
        print('staining_set :', s_staining_set)
        for s_staining in dd_staining[s_staining_set].keys():
            print('staining_set, staining :', s_staining_set, s_staining)
            for t_staining in dd_staining[s_staining_set][s_staining]:
                print('staining_set, staining, t_staining :', s_staining_set, s_staining,t_staining)

    print('\n*** selftest d_plate_layout ***')
    for s_key in d_plate_layout:
        print('key and value:', s_key, d_plate_layout[s_key])
